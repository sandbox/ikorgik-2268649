<?php

/**
 * @file
 * Provides integration with Rules module.
 */

/**
 * Implements hook_rules_action_info().
 */
function billomat_rules_action_info() {

  $actions['billomat_action_invoice_create_from_order'] = array(
    'label' => t('Create billomat invoice from order'),
    'group' => t('Billomat'),
    'parameter' => array(
      'client_id' => array(
        'type' => 'integer',
        'label' => t('Client id'),
      ),
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'provides' => array(
      'invoice_id' => array(
        'type' => 'integer',
        'label' => t('Created invoice id'),
      ),
    ),
  );
  $actions['billomat_action_invoice_complete'] = array(
    'label' => t('Complete billomat invoice'),
    'group' => t('Billomat'),
    'parameter' => array(
      'invoice_id' => array(
        'type' => 'integer',
        'label' => t('Invoice id'),
      ),
    ),
  );
  $actions['billomat_action_invoice_get_pdf'] = array(
    'label' => t('Get invoice pdf'),
    'group' => t('Billomat'),
    'parameter' => array(
      'invoice_id' => array(
        'type' => 'integer',
        'label' => t('Invoice id'),
      ),
    ),
    'provides' => array(
      'returned_pdf_file' => array(
        'type' => 'file',
        'label' => t('Returned pdf file'),
      ),
    ),
  );
  $actions['billomat_action_client_create'] = array(
    'label' => t('Create billomat client'),
    'group' => t('Billomat'),
    'parameter' => array(
      'name' => array(
        'type' => 'text',
        'label' => t('Client name'),
      ),
      'street' => array(
        'type' => 'text',
        'label' => t('Street'),
        'optional' => TRUE,
      ),
      'zip' => array(
        'type' => 'text',
        'label' => t('Zip'),
        'optional' => TRUE,
      ),
      'city' => array(
        'type' => 'text',
        'label' => t('City'),
        'optional' => TRUE,
      ),
      'country_code' => array(
        'type' => 'text',
        'label' => t('Country code'),
        'optional' => TRUE,
      ),
    ),
    'provides' => array(
      'client_id' => array(
        'type' => 'integer',
        'label' => t('Created client id'),
      ),
    ),
  );

  return $actions;
}

/**
 * Creates an invoice for client with given id from given order.
 *
 * @param integer $client_id
 *   Billomat client id for which invoice will be created.
 * @param object $order
 *   An Order from which invoice will be created.
 *
 * @return integer|FALSE
 *   Returns ID of created Invoice or FALSE otherwise.
 */
function billomat_action_invoice_create_from_order($client_id, $order) {
  _billomat_load_include('invoice', 'resources');

  // Load an order object.
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $commerce_line_items = $wrapper->commerce_line_items->value();

  // Get all line items from the order.
  $invoice_items = array();
  foreach ($commerce_line_items as $commerce_line_item) {
    $line_wrapper = entity_metadata_wrapper('commerce_line_item', $commerce_line_item);

    $tax_rate = '';
    $unit_price = '';
    $commerce_unit_price = $line_wrapper->commerce_unit_price->value();
    foreach ($commerce_unit_price['data']['components'] as $component) {

      // Get item base price.
      if ($component['name'] == 'base_price') {
        $unit_price = $component['price']['amount'] / 100;
      }

      // Get VAT from the line item.
      if (module_enable(array('commerce_vat'))) {

        $component_name = explode('|', $component['name']);
        if ($component_name[0] == 'vat') {
          $tax_rate = $component['price']['data']['vat_rate_info']['rate'] * 100;
        }
      }
    }

    $invoice_items[] = array(
      'unit_price' => $unit_price,
      'quantity' => $line_wrapper->quantity->value(),
      'title' => $line_wrapper->commerce_product->title->value(),
      'tax_rate' => $tax_rate,
    );
  }

  // Create invoice.
  if ($response = billomat_invoice_create($client_id, $invoice_items)) {

    return array(
      'invoice_id' => $response->invoice->id,
    );
  }

  return FALSE;
}

/**
 * Complete Billomat invoice by given id.
 *
 * @param integer $invoice_id
 *   Billomat invoice id what will be completed.
 *
 * @return bool
 *   Returns FALSE if any problems.
 */
function billomat_action_invoice_complete($invoice_id) {
  _billomat_load_include('invoice', 'resources');

  return billomat_invoice_complete($invoice_id);
}


/**
 * Generates Drupal file object from the billomat invoice pdf.
 *
 * @param integer $invoice_id
 *   Billomat invoice id from which pdf will be returned.
 *
 * @return object|FALSE
 *   File object with pdf from invoice or FALSE otherwise.
 */
function billomat_action_invoice_get_pdf($invoice_id) {
  global $user;
  _billomat_load_include('invoice', 'resources');

  $response = billomat_invoice_get_pdf($invoice_id);
  $pdf = $response->pdf;

  $uri = file_unmanaged_save_data(base64_decode($pdf->base64file), 'public://' . $pdf->filename);
  if ($uri) {

    // Creates file object from returned file.
    $file = new stdClass;
    $file->status = 1;
    $file->uri = $uri;
    $file->uid = $user->uid;
    $file->filesize = $pdf->filesize;
    $file->filename = $pdf->filename;
    $file->filemime = $pdf->mimetype;

    file_save($file);
    return array(
      'returned_pdf_file' => $file,
    );
  }

  return FALSE;
}

/**
 * Creates Billomat client by given parameters.
 *
 * @param string $name
 *   Company name.
 * @param string $country_code
 *   Country code as ISO 3166.
 *
 * @return integer|bool
 *   Returns created client id or FALSE otherwise.
 */
function billomat_action_client_create($name, $street, $zip, $city, $country_code) {
  _billomat_load_include('client', 'resources');

  if ($response = billomat_client_create($name, $street, $zip, $city, $country_code)) {

    return array(
      'client_id' => $response->client->id,
    );
  }

  return FALSE;
}