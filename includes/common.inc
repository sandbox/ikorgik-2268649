<?php

/**
 * @file
 * Common functions for billomat module.
 */

/**
 * Compose url for the request.
 *
 * @param string $resource_url
 *   Url of the resource for which request will be executed.
 *
 * @return bool|string
 *    Returns complete url for the request or FALSE if billomat isn't configured.
 */
function _billomat_prepare_url($resource_url) {

  // Get billomat settings.
  $billomat_id = variable_get('billomat_id', '');
  $api_key = variable_get('billomat_api_key', '');
  $protocol = variable_get('billomat_protocol', 'http');

  // Returns an error if billomat isn't configured.
  if (empty($billomat_id) || empty($api_key)) {

    drupal_set_message(t('Billomat isn\'t configured.'), 'error');
    watchdog('billomat', 'Billomat isn\'t configured.');

    return FALSE;
  }
  else {
    // Compose url from given parameters.
    $path = sprintf('%s://%s.billomat.net/api/%s', $protocol, $billomat_id, $resource_url);
    $query = array(
      'api_key' => $api_key,
      'format' => 'json',
    );

    return $path . '?' . drupal_http_build_query($query);
  }
}

/**
 * Performs an HTTP request.
 *
 * @param $ressource_url
 *   A string containing a fully qualified URI.
 * @param string $method
 *   A string containing the request method. Defaults to 'GET'.
 * @param array $data
 *   An array containing parameters for the request.
 *
 * @return bool|object
 *   Returns response data object or FALSE otherwise.
 */
function _billomat_http_request($ressource_url, $method = 'GET', $data = array()) {

  // Compose url.
  $url = _billomat_prepare_url($ressource_url);

  if (!$url) {
    return FALSE;
  }

  $options = array(
    'headers' => array(
      'Content-Type' => 'application/json',
    ),
    'method' => $method,
    'data' => json_encode($data),
  );

  // Perform request.
  $response = drupal_http_request($url, $options);
  $response_data = json_decode($response->data);

  if (!empty($response->code) && ($response->code == 200 || $response->code == 201)) {

    return empty($response_data) ? TRUE : $response_data;
  }
  elseif (!empty($response_data->errors->error)) {

    drupal_set_message(t('Billomat: ') . $response_data->errors->error, 'error');
    watchdog('billomat', 'Billomat: ' . $response_data->errors->error);
  }

  return FALSE;
}
