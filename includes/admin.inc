<?php

/**
 * @file
 * Administration settings for the billomat module.
 */

/**
 * Callback for billomat settings page.
 */
function _billomat_settings_form($form, &$form_state) {

  $form['billomat_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Billomat ID'),
    '#default_value' => variable_get('billomat_id', ''),
    '#description' => t('Your billomatID registered on billomat.net site'),
    '#required' => TRUE,
  );
  $form['billomat_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('billomat_api_key', ''),
    '#description' => t('You can take in your billomat account. For more info see http://www.billomat.com/en/api/basics/authentication'),
    '#required' => TRUE,
  );
  $form['billomat_template_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Template id'),
    '#default_value' => variable_get('billomat_template_id', ''),
    '#description' => t('Template id chosen for invoices, you can take it in your billomat account: Settings -> Documents -> Template'),
    '#required' => TRUE,
  );
  $form['billomat_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Protocol'),
    '#options' => array('http' => 'http', 'https' => 'https'),
    '#description' => t('Can be http or https.'),
    '#default_value' => variable_get('billomat_protocol', ''),
  );

  return system_settings_form($form);
}

