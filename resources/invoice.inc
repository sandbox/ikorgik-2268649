<?php

/**
 * @file
 * Functions for work with invoice resource.
 */

/**
 * Creates an invoice for client with given id from given order.
 *
 * @param integer $client_id
 *   Billomat client id for which invoice will be created.
 * @param array $invoice_items
 *   Invoice items what will be added to the invoice. An array of items each contains:
 *    -unit_price: Product price.
 *    -quantity: Quantity of products.
 *    -title: Product title.
 *
 * @return object|bool
 *   Returns response data object or FALSE otherwise.
 */
function billomat_invoice_create($client_id, $invoice_items = array()) {

  $data = array(
    'invoice' => array(
      'client_id' => $client_id,
      'invoice-items' => array(
        'invoice-item' => $invoice_items,
      ),
    )
  );

  return _billomat_http_request('invoices', 'POST', $data);
}

/**
 * Complete Billomat invoice by given id.
 *
 * @param integer $invoice_id
 *   Billomat invoice id what will be completed.
 *
 * @return bool
 *   Returns FALSE if any problems.
 */
function billomat_invoice_complete($invoice_id) {

  $template_id = variable_get('billomat_template_id', '');

  $data = array(
    'complete' => array(
      'template_id' => $template_id,
    )
  );

  $ressource_url = sprintf('invoices/%d/complete', $invoice_id);
  return _billomat_http_request($ressource_url, 'PUT', $data);
}

/**
 * Generates Drupal file object from the billomat invoice pdf.
 *
 * @param integer $invoice_id
 *   Billomat invoice id from which pdf will be returned.
 *
 * @return object|bool
 *   Returns response data object or FALSE otherwise.
 */
function billomat_invoice_get_pdf($invoice_id) {
  $ressource_url = sprintf('invoices/%d/pdf', $invoice_id);

  return _billomat_http_request($ressource_url);
}
