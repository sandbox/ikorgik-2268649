<?php

/**
 * @file
 *  Functions for work with client resource.
 */

/**
 * Creates Billomat client by given parameters.
 *
 * @param string $name
 *   Company name.
 * @param string $country_code
 *   Country code as ISO 3166.
 *
 * @return object|bool
 *   Returns response data object or FALSE otherwise.
 */
function billomat_client_create($name, $street = '', $zip = '', $city = '', $country_code = '') {

  $data = array(
    'client' => array(
      'name' => $name,
      'street' => $street,
      'zip' => $zip,
      'city' => $city,
      'country_code' => $country_code,
    )
  );

  return _billomat_http_request('clients', 'POST', $data);
}

